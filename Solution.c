/*
 ========================================
 *Project: Assignment1_Pb8-10ff
 *The File Name: Solution.c
 *Created by : Ahmed Fatouh
 *Jul 25, 2019
 *3:34:40 AM
 ========================================
 */

/*
 ========================================
 *Project: Assignment1_Pb8-10
 *The File Name: Solution.c
 *Created by : Ahmed Fatouh
 *Jul 25, 2019
 *3:23:32 AM
 ========================================
 */

#include<stdio.h>

char isprime(int x) {
	if (x < 0)
		x = x * -1;
	if (x == 0 || x == 1)
		return 'f';
	for (int i = x - 1; i > 1; i--) {
		if (x % i == 0)
			return 'f';
	}
	return 't';
}

void display_prime(int x, int y) {
	printf("Prime Numbers are: ");
	while (x <= y) {
		if (isprime(x) == 't') {
			printf("%d\t", x);
		}
		x++;
	}
	printf("\n\n");
}

void pyramid(int x) {
	if (x % 2 == 0) {
		x--;
	}
	int in = (x - 1) / 2;
	int of = in;
	for (int i = 0; i <= (x - 1) / 2; i++) {
		for (int j = 0; j <= of; j++) {
			if (j >= in)
				printf("*");
			else
				printf(" ");
		}
		printf("\n");
		in--;
		of++;
	}

}
void swap(int *x, int *y) {
	int t = *x;
	*x = *y;
	*y = t;
}
int main() {
	int x = 2;
	int y = 20;
	display_prime(x, y);
	swap(&x, &y);
	printf("%d\t%d\n\n", x, y);
	pyramid(10);
	return 0;
}

